package cn.ewsd.system;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class ServerInfoApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ServerInfoApplication.class);
        application.setBannerMode(Banner.Mode.CONSOLE);
        application.run(args);
	}
}
